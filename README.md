### Carbon helper function

`carbon('2018-04-18')` returns a carbon instance

- addWorkdays($count): self
- isHoliday()
- isWorkday()

'isLiberationDay'            
'isRepublicDay'            
'isImmaculateConceptionFeast'
'isAssumptionOfMaryFeast'    
'isEpiphany'                
'isDayBeforeChristmas'      
'isChristmas'               
'isSaintStephenDay'          
'isSaintSylvesterDay'        
'isWorkersDay'             
